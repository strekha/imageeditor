package com.strekha.imageeditor.test

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import com.jsibbold.zoomage.ZoomageView

class DrawingView : ZoomageView {

    constructor(context: Context?) : super(context)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)

    private var paint = Paint().apply {
        color = Color.RED
        isAntiAlias = true
        style = Paint.Style.STROKE
        strokeWidth = 15f
        strokeJoin = Paint.Join.ROUND
        strokeCap = Paint.Cap.ROUND
    }

    var color = Color.RED
        set(value) {
            paint.color = value
        }

    private var isScaling = false
    private var tempPath: Path? = null
    private val graphics = mutableListOf<Pair<Path, Paint>>()
    private var paintMatrix = Matrix()

    override fun onTouchEvent(event: MotionEvent?): Boolean {

        val action = event?.action
        val x = event?.x ?: 0f
        val y = event?.y ?: 0f

        if (event?.actionMasked == MotionEvent.ACTION_POINTER_DOWN || event?.actionMasked == MotionEvent.ACTION_DOWN) {
            if (event.pointerCount == 1) {
                tempPath = Path()
                tempPath?.moveTo(x, y)
            } else {
                tempPath = null
            }
        } else if (!isScaling && action == MotionEvent.ACTION_MOVE) {
            tempPath?.let {
                it.lineTo(x, y)
                invalidate()
                return true
            }
        } else if (action == MotionEvent.ACTION_UP && tempPath != null) {
            graphics.add(Pair(tempPath!!, paint))
            tempPath = null
            invalidate()
            initPaint()
        }
        return super.onTouchEvent(event)
    }

    override fun onDraw(canvas: Canvas?) {
        if (canvas != null) {
            for ((path, paint) in graphics) {
                canvas.drawPath(path, paint)
            }
            if (tempPath != null) canvas.drawPath(tempPath, paint)
        }
    }

    override fun onScaleBegin(detector: ScaleGestureDetector?): Boolean {
        isScaling = true
        return super.onScaleBegin(detector)
    }

    override fun onScaleEnd(detector: ScaleGestureDetector?) {
        isScaling = false
        super.onScaleEnd(detector)
    }

    override fun onScale(detector: ScaleGestureDetector?): Boolean {
        if (detector != null) {
//            paintMatrix.setScale(scaleBy, scaleBy, detector.focusX, detector.focusY)
//            val xdistance = getXDistance(detector.focusX, last.x)
//            val ydistance = getYDistance(detector.focusY, last.y)
//            paintMatrix.postTranslate(xdistance, ydistance)
            transformPaths(paintMatrix)
        }
        return super.onScale(detector)
    }

    private fun initPaint() {
        val newPaint = Paint(paint)
        paint = newPaint
    }

    private fun transformPaths(matrix: Matrix) {
        val matrixValues = FloatArray(9)
        matrix.getValues(matrixValues)
        graphics.forEach {
            it.first.transform(matrix)
            val width = it.second.strokeWidth
            it.second.strokeWidth = width * matrixValues[Matrix.MSCALE_X]
        }
    }
}