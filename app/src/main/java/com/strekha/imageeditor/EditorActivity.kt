package com.strekha.imageeditor

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import com.bumptech.glide.Glide
import com.strekha.imageeditor.test.ZoomView
import com.thebluealliance.spectrum.SpectrumDialog

class EditorActivity : AppCompatActivity() {

    private val editorView by bind<ZoomView>(R.id.imageView)
    private val colorChooser by bind<Button>(R.id.color_chooser)

    companion object {
        fun newIntent(context: Context, uri: Uri): Intent {
            val intent = Intent(context, EditorActivity::class.java)
            intent.data = uri
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editor)

        Glide.with(applicationContext).load(intent.data).into(editorView)

        /*colorChooser.setOnClickListener {
            SpectrumDialog.Builder(this)
                    .setOnColorSelectedListener { _, color -> editorView.color = color }
                    .setColors(R.array.colors)
                    .setSelectedColor(editorView.color)
                    .setOutlineWidth(2)
                    .build()
                    .show(supportFragmentManager, "ColorPicker")
        }*/
    }
}
